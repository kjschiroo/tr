use std::collections::HashMap;
use std::fs;
use std::path::{Path, PathBuf};
use std::process::Command;

use toml;

#[derive(Debug)]
pub struct Config {
    commands: HashMap<String, String>,
    variables: HashMap<String, String>,
}

pub fn coalesce_config(path: &Path) -> Result<Config, String> {
    let config_paths = get_config_files(path);
    let mut config = Config {
        commands: HashMap::new(),
        variables: HashMap::new(),
    };
    for config_path in config_paths {
        let local_config = read_config_from_file(&config_path)?;
        config.commands.extend(local_config.commands);
        config.variables.extend(local_config.variables);
    }
    Ok(config)
}

fn read_config_from_file(path: &PathBuf) -> Result<Config, String> {
    let contents = match fs::read_to_string(path) {
        Ok(contents) => {contents},
        Err(_) => return Err(format!("Could not read file: {:?}", path.as_os_str()))
    };
    let raw_toml = match contents.parse::<toml::Value>() {
        Ok(raw_toml) => {raw_toml},
        Err(_) => return Err(format!("Invalid TOML: {:?}", path.as_os_str()))
    };
    Ok(toml_to_config(raw_toml))
}

fn toml_to_config(raw_toml: toml::Value) -> Config {
    let commands = match raw_toml.get("commands") {
        Some(toml::Value::Table(value_map)) => to_hash_map(value_map),
        _ => HashMap::new(),
    };
    let variables = match raw_toml.get("variables") {
        Some(toml::Value::Table(value_map)) => to_hash_map(value_map),
        _ => HashMap::new(),
    };
    Config {commands, variables}
}


pub fn execute_command(args: &Vec<String>, config: &Config) -> Result<(), String> {
    if args.len() == 0 {
        return Err("No command given".to_string());
    }
    let command = match config.commands.get(&args[0]) {
        Some(value) => value,
        None => return Err(format!("Unknown command: \"{}\"", &args[0]))
    };
    println!("{}", command);
    match Command::new("bash")
        .args(&["-c", command])
        .envs(&config.variables)
        .status()
    {
        Err(why) => return Err(format!("Could not spawn process: {}", why)),
        _ => return Ok(()),
    };
}

fn get_config_files(deepest_path: &Path) -> Vec<PathBuf> {
    let mut result = vec![];
    let mut optional = Some(deepest_path);
    while let Some(working_path) = optional {
        let candidate = PathBuf::from(working_path.join(".tr.toml"));
        if candidate.exists() {
            result.push(candidate);
        }
        match working_path.parent() {
            Some(path) => {
                optional = Some(path);
            }
            None => {
                optional = None;
            }
        }
    }
    result.reverse();
    result
}

fn to_hash_map(toml_hash: &toml::map::Map<String, toml::Value>) -> HashMap<String, String> {
    let mut result = HashMap::new();
    for (key, value) in toml_hash.iter() {
        match value {
            toml::Value::String(command) => {
                result.insert(key.to_string(), command.to_string());
            }
            _ => {}
        }
    }
    result
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::File;
    use std::fs::create_dir;
    use std::io::prelude::*;
    use tempfile::tempdir;

    #[test]
    fn test_coalesce_config() {
        let dir = tempdir().unwrap();
        let child_dir = dir.path().join("child");
        create_dir(&child_dir).unwrap();
        let mut base_config = File::create(dir.path().join(".tr.toml")).unwrap();
        let mut child_config = File::create(child_dir.join(".tr.toml")).unwrap();
        base_config
            .write_all(
                "[commands]\n\
                logs = \"aws logs $LOG_PATH\"\n\
                foo = \"baz\""
                    .as_bytes(),
            )
            .expect("Could not write base config");
        child_config
            .write_all(
                "[commands]\n\
                 foo = \"bar\"\n\
                 [variables]\n\
                 LOG_PATH = \"tr\""
                    .as_bytes(),
            )
            .expect("Could not write child config");

        let config = coalesce_config(&child_dir).unwrap();

        assert_eq!(config.commands.get("foo"), Some(&"bar".to_string()));
        assert_eq!(
            config.commands.get("logs"),
            Some(&"aws logs $LOG_PATH".to_string())
        );
        assert_eq!(config.variables.get("LOG_PATH"), Some(&"tr".to_string()));
    }

    #[test]
    fn test_execute_command() {
        let mut commands = HashMap::new();
        commands.insert("hello".to_string(), "echo \"Hello ${name}!\"".to_string());
        let mut variables = HashMap::new();
        variables.insert("name".to_string(), "World".to_string());
        let config = Config {commands, variables};
        let args = vec!["hello".to_string()];

        execute_command(&args, &config).unwrap();
    }
}
