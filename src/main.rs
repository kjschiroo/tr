use std::env;
use tr::coalesce_config;
use tr::execute_command;

fn main() {
    let config = coalesce_config(&env::current_dir().unwrap()).unwrap();
    let args: Vec<String> = env::args().collect();
    match execute_command(&args[1..].to_vec(), &config) {
        Err(message) => eprintln!("{}", message),
        _ => {}
    };
}
