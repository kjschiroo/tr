# TR

A basic Task Runner.

## Install

### Build from source

```
cargo build --release
```

## Usage

Declare commands within a `.tr.toml` file.

```toml
[commands]
sync = "rsync -a ./source username@remote_host:destination"
logs = "log-puller my-log-id"
```

To run a command:

```
> tr sync
rsync -a ./source username@remote_host:destination
```

### Advanced Usage

Multiple `.tr.toml` files can be used in combination to avoid repetition.
`.tr.toml` files will be layered on top of each other with those lowest in
the directory structure taking priority over those higher in the directory
structure.

With a configuration like this in ones home directory:

```toml
# ~/.tr.toml
[commands]
logs = "log-puller $LOG_ID"
```

and another like this in a project directory underneath:

```toml
# ~/projects/my-project-repo
[variables]
LOG_ID = "my-log-id
```

They are interpreted together to the equivalent of:

```toml
[commands]
logs = "log-puller $LOG_ID"

[variables]
LOG_ID = "my-log-id
```
